# konghead

## electronics mounting plate for Tamiya G6-01 chassis, as found on the Konghead!

[<img src="https://www.tamiyausa.com/media/CACHE/images/products/rc-118-konghead-6x6-g6-01-13-225c/b5058e8b0a4f8c71ca266db2877389be.jpg" width="480">](https://www.tamiyausa.com/shop/110-trucks/rc-118-konghead-6x6/)

# Release History
[Project overview->Releases](https://gitlab.com/openscad1/konghead/-/releases)


