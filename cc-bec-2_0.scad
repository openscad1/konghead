fine = true;
//fine = false;


echo("$fn: ", $fn);

include <lib/konghead_values.scad>

thickitude = 2;

rounditude = fine ? 0.25 : 0;

fudgitude = fine ? 0.05 : 0;

plate_thickness = po8 * thickitude;






w = 35.5;
l = 17.8; 
h = 11.65;
//d=1;
d = 1;
r = d/2;

lip = 1.5;
/*


difference() {
    
    color("orange") {
        minkowski() {
            difference() {
                translate([r, r, r]) {
                    cube([w + (po8 * 2) - d, l + (po8 * 2) - d, h + lip + lip + (po8 * 0) - d]); 
                }
                more = 40;
                translate([((w + (po8 * 2)) / 4), 0, 0]) {
                    color("red") cube([more, more, more]);
                }
            }
            sphere(d = d);
        }
    }


    translate([po8, po8, po8 * + lip]) {
        color("lightblue") {
            minkowski() {
                translate([r+lip, r+lip, r]) {
                    cube([(w - d) - (lip * 2), (l - d) - (lip * 2), (h + (po8 * 2)) - d ]);
                }
                sphere(d = d);
            }
        }
    }
    translate([po8, po8, po8 * (-2)]) {
        color("lightblue") {
            minkowski() {
                translate([r, r, r+lip+po8*2]) {
                    cube([(w - d), (l - d), (h + (po8 * 0)) - d ]);
                }
                sphere(d = d);
            }
        }
    }
    translate([0, po8, lip]) {
        cylinder(h=h, d=po8);
    }
    translate([0, l + po8, lip]) {
        cylinder(h=h, d=po8);
    }
}



pegs = 4;





module peg(rounditude = rounditude, fudgitude = fudgitude) {
    color("magenta")
    translate([(rounditude + fudgitude) - (po8 / 2), rounditude + fudgitude, rounditude + fudgitude]) {
        minkowski() {
            cube([po8 + (po8 / 2) - ((rounditude + fudgitude) * 2), po8 - ((rounditude + fudgitude) * 2), (po8/2) - ((rounditude + fudgitude) * 2)]);
            sphere(r = rounditude);
        }
    }
}

module comb(rounditude = rounditude, fudgitude = fudgitude) {
    for(p = [0:pegs-1]) {
        translate([0, po8 * p * 2, 0]) peg(rounditude, fudgitude);
    }
}

padding = (4.7 / 2) + (w - 35.5);
translate([po8 + padding, ((l + (po8 * 2)) - (po8 * ((pegs * 2) - 1))) / 2, -po8*2]) {

    translate([0, 0, -fudgitude]) {
        comb();
    }
    
    color("lightgreen") translate([-(po8+padding), 0, 0]) {
        translate([r, r, r]) {
            minkowski() {
                cube([po8+padding-d, po8 * ((pegs * 2) - 1)-d, po8 * 2+1-d]);
                sphere(d=d);
            }
        }
    }

    color("lightblue") translate([0, 0, (po8*1.5) + fudgitude]) comb();
    color("blue") translate([0, 0, (po8*1.5) + fudgitude + (0.5)]) comb();

}






*/
use <moscube/moscube.scad>;

module post() {
        translate([0, 0, po8/2]) {
        sphere(d=po8);
        cylinder(h=h - po8, d=po8);
        translate([0, 0, h-po8]) {
            sphere(d=po8);
        }
    }
}

translate([0, 0, lip]) {
  
    more = 6.4;

    
    translate([po8+w+more,  2 * po8, 0]) post();
    translate([    w+more,      po8, 0]) post();


    translate([0, -po8, 0]) {
        moscube(x = po8, y = po8 * 3, z = h, roundcorner_radius = rounditude, x_anchor = -1, y_anchor = 1, z_anchor = 1);
    }
    translate([-po8, 0, 0]) {
        moscube(x = w + (po8 * 2) + more, y = po8, z = h, roundcorner_radius = rounditude, x_anchor = 1, y_anchor = -1, z_anchor = 1);
    }
    translate([(w + po8)+more, -po8, 0]) {
        moscube(x = po8, y = po8 * 3, z = h, roundcorner_radius = rounditude, x_anchor = -1, y_anchor = 1, z_anchor = 1);
    }

    translate([   0,      po8, 0]) post();
    translate([-po8, 2 * po8, 0]) post();

}

    
