fine = true;
//fine = false;

$fn = fine ? 50 : 25;

echo("$fn: ", $fn);

po8 = 25.4 / 8;

thickitude = 2;

rounditude = fine ? 0.25 : 0;



plate_width = 30;

plate_length = 84;


plate_thickness = po8 * thickitude;

hanger_width = 6.875;

// set to zero for final render
//flash = 0;
flash = fine ? 0 : 0.001;

chevron_angle = 170;

// use two right triangles to make the chevron
// calculate the length of the short side of the right triangle to find the apex of the chevron
A = chevron_angle / 2;
B = 90;
C = B - A;

a = plate_width / 2;
b = sin(B) * (a/sin(A));
c = sin(C) * (a/sin(A));


echo("length a: ", a);
echo("angle A: ", A);

echo("length b: ", b);
echo("angle B: ", B);

echo("length c: ", c);
echo("angle C: ", C);

echo("flash: ", flash);
echo("rounditude: ", rounditude);


module fishbone(length = plate_length, rounditude = 0, bottom_teeth = false) {
    minkowski() {
        difference() {
            translate([0, 0, rounditude]) {
                linear_extrude(height = plate_thickness - (rounditude * 2)) {
                    polygon(
                        points = [
                            [rounditude, rounditude],
                            [a, c + rounditude],
                            [(a * 2) - rounditude, rounditude],
                            [(a * 2) - rounditude, length - rounditude],
                            [rounditude, length - rounditude]
                        ]
                    );
                }
            }

            if(bottom_teeth) {
                for(offset = [po8 * 2 : po8 * 2 : length - (po8 * 1)]) {
                    translate([0, offset, 0]) {
                        tb = 0;
                        for(lr = [0 , plate_width - po8 ]) {
                            translate([lr - flash, -flash, (po8 * tb) - flash]) {
                                translate([-rounditude, -rounditude, -rounditude]) {
                                    cube([po8 + (flash * 2) + (rounditude * 2), po8 + (flash * 2) + (rounditude * 2), po8/2 + (flash * 2) + (rounditude * 2)]);
                                }
                            }
                        }
                    }
                }
            }

            for(offset = [po8 * 2 : po8 * 2 : length - (po8 * 1)]) {
                translate([0, offset, 0]) {                   
                    tb = thickitude - 0.5;
                    for(lr = [0 , plate_width - po8 ]) {
                        translate([lr - flash, -flash, (po8 * tb) - flash]) {
                            translate([-rounditude, -rounditude, -rounditude]) {
                                cube([po8 + (flash * 2) + (rounditude * 2), po8 + (flash * 2) + (rounditude * 2), po8/2 + (flash * 2) + (rounditude * 2)]);
                            }
                        }
                    }
                }
            }
            
            
        }
        
        sphere(r = rounditude);
    }

}



// final diameter: 
hanger_thru_hole = 2.875;

// testing: hanger_thru_hole = 2.5;

module hanger() {
    translate([hanger_width / 2, 0, -(4.5 + 0.6)]) {
        rotate([90,0,90]) {
            difference() {
                union() {
                    cylinder(d = 7.5, h = hanger_width, center = true);
                    translate([0, (7.5 / 2), 0]) {
                        cube([7.5, 7.5, hanger_width], center = true);
                    }
                    translate([0, 7.5, 0]) {
                        cube([7.5, plate_thickness, hanger_width], center = true);
                    }
                }
                cylinder(d = hanger_thru_hole, h = hanger_width + (flash * 2), center = true);
            }
        }
    }
}


module tube() {
    translate([tube_diameter / 2, tube_length + (tube_diameter / 2), tube_diameter / 2]) {
        rotate([90, 0, 0]) {
            difference() {
                union() {
                    sphere(d = tube_diameter);
                    linear_extrude(height = tube_length) {
                        circle(d = tube_diameter);
                    }
                    translate([0, 0, tube_length]) {
                        sphere(d = tube_diameter);
                    }
                }
                cylinder(h=200, d=2, center = true);
            }
        }
    }
}





hanger_offset = 78 - 1.25;
module hangers() {
    translate([0, hanger_offset, -2]) {
        hanger();
        translate([plate_width - hanger_width, 0, 0]) {
            hanger();
        }
    }
}


tube_diameter = plate_thickness;
tube_length = 5;

module tubes() {
    difference() {
        union() {
            translate([-(tube_diameter / 2), -1 * (tube_length), 0]) {
                translate([-0.5, 0, 0]) tube();
                translate([plate_width, 0, 0]) {
                    translate([0.5, 0, 0]) tube();
                }
            }
        }
        translate([-(plate_width * 19) / 2, -(24.9345 + 2.5), 0]) {
            chevron(apex_angle = chevron_angle, height = plate_thickness * 2, base_length = plate_width * 20);
        }
    }
}




module chevron(apex_angle = 160, base_length = 10, height = 5) {
    chevron_angle = 170;

    // use two right triangles to make the chevron
    // calculate the height of the chevron (middle of long side to apex of chevron) 
    // as the short side of the two right triangles butted to each other at the apex

    A = apex_angle / 2;
    B = 90;
    C = B - A;

    a = base_length / 2;
    b = sin(B) * (a/sin(A));
    c = sin(C) * (a/sin(A));

    echo("chevron apex height: ", c);

    linear_extrude(height = height) {
        polygon(
            points = [
                        [0, 0],
                        [a, c],
                        [(a * 2), 0]
            ]
        );
    }
}


difference() {
    union() {
        difference() {

            color("orange") fishbone(length = plate_length + 49, rounditude = rounditude, bottom_teeth = true);

            

            translate([0, 13.25, -4]) 
            color("lightgreen") {
                translate([0, -1.8, 0]) {
                    translate([(plate_width * 21) / 2, plate_length, 0]) {
                        rotate([15, 0, 180]) {
                            chevron(apex_angle = 175, height = plate_thickness * 2, base_length = plate_width * 20);
                        }
                    }
                    translate([(plate_width * 21) / 2, plate_length + 13, 0]) {
                        rotate([-15, 0, 180]) {
                            chevron(apex_angle = 175, height = plate_thickness * 2, base_length = plate_width * 20);
                        } 
                    }
                }
            }

            translate([plate_width / 2, 0, -2])         rotate([90, 0, 180])             cylinder(h=200, d=plate_width - (hanger_width * 2));

        }
        translate([0, 0, plate_thickness * 1]) color("pink") fishbone(length = plate_length + 49, rounditude = rounditude, bottom_teeth = true);

        color("cyan") tubes();
    }
    color("purple") translate([plate_width / 2, 0, -4]) rotate([-5,0,0]) cube([plate_width * 2,20,10], center = true);

}



color("red") hangers();




/*


module peg() {
    color("magenta")
    translate([rounditude, rounditude, rounditude]) {
        minkowski() {
            cube([po8 - (rounditude * 2), po8 - (rounditude * 2), po8 - (rounditude *2)]);
            sphere(r = rounditude);
        }
    }
}

peg();
translate([0, po8 * 2, 0]) peg();
translate([0, po8 * 4, 0]) peg();
color("lightgreen") translate([-po8, 0, 0]) cube([po8, po8 * 5, po8 * 3]);
color("lightblue") translate([0, 0, po8*2]) cube([po8, po8 * 5, po8]);

*/
