fine = true;
//fine = false;


echo("$fn: ", $fn);

include <lib/konghead_values.scad>

thickitude = 2;

rounditude = fine ? 0.25 : 0;

fudgitude = fine ? 0.05 : 0;

plate_thickness = po8 * thickitude;




pegs = 4;





module peg(rounditude = rounditude, fudgitude = fudgitude) {
    color("magenta")
    translate([(rounditude + fudgitude) - (po8 / 2), rounditude + fudgitude, rounditude + fudgitude]) {
        minkowski() {
            cube([po8 + (po8 / 2) - ((rounditude + fudgitude) * 2), po8 - ((rounditude + fudgitude) * 2), (po8/2) - ((rounditude + fudgitude) * 2)]);
            sphere(r = rounditude);
        }
    }
}

module comb(rounditude = rounditude, fudgitude = fudgitude) {
    for(p = [0:pegs-1]) {
        translate([0, po8 * p * 2, 0]) peg(rounditude, fudgitude);
    }
}


translate([0, 0, -fudgitude]) comb();
color("lightgreen") translate([-po8, 0, 0]) cube([po8, po8 * ((pegs * 2) - 1), po8 * 2]);
color("lightblue") translate([0, 0, (po8*1.5) + fudgitude]) comb();


